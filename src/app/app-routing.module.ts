import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '', redirectTo: 'welcome', pathMatch: 'full'
  },
  { 
    path: 'login', 
    loadChildren: './pages/login/login.module#LoginPageModule' 
  },
  { 
    path: 'welcome', 
    loadChildren: './pages/welcome/welcome.module#WelcomePageModule' 
  },
  { 
    path: 'diary', 
    canActivate: [AuthGuardService],
    loadChildren: './pages/diary/diary.module#DiaryPageModule' 
  },
  { 
    path: 'diary/:packet', 
    canActivate: [AuthGuardService],
    loadChildren: './pages/user-packet/user-packet.module#UserPacketPageModule' 
  },
  { 
    path: 'il-tuo-tour', 
    loadChildren: './pages/il-tuo-tour/il-tuo-tour.module#IlTuoTourPageModule' 
  },
  { 
    path: 'companies', 
    loadChildren: './pages/companies/companies.module#CompaniesPageModule' 
  },
  { 
    path: 'il-tuo-tour/:cluster', 
    loadChildren: './pages/cluster/cluster.module#ClusterPageModule' 
  },
  { 
    path: 'il-tuo-tour/:cluster/:packet', 
    loadChildren: './pages/packet/packet.module#PacketPageModule' 
  },
  { 
    path: 'il-tuo-tour/:cluster/:packet/prenota', 
    loadChildren: './pages/prenota-tour/prenota-tour.module#PrenotaTourPageModule' 
  },
  { 
    path: 'companies/:company', 
    loadChildren: './pages/company/company.module#CompanyPageModule' 
  },
  { 
    path: 'prenota-tour', 
    loadChildren: './pages/prenota-tour/prenota-tour.module#PrenotaTourPageModule' 
  },
  { 
    path: 'footer', 
    loadChildren: './pages/footer/footer.module#FooterPageModule' 
  },
  { 
    path: 'poi/:poi', 
    loadChildren: './pages/poi/poi.module#PoiPageModule' 
  }

];

@NgModule({
  imports: [
    FormsModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    //RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
