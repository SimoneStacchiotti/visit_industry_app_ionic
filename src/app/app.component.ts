import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { SwUpdate } from '@angular/service-worker';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public diaryPage = {
    title: 'Diario di viaggio',
    url: '/diary',
    icon: 'book'
  }

  public appPages = [
    {
      title: 'Home',
      url: '/welcome',
      icon: 'home'
    },
    {
      title: 'Il tuo tour',
      url: '/il-tuo-tour',
      icon: 'subway',
      clusters: [
        {
          name: "Luce",
          url: "/il-tuo-tour/Luce",
          color: "#fcc600"
        }, 
        {
          name: "Suono",
          url: "/il-tuo-tour/Suono",
          color: "#e43887"
        }, 
        {
          name: "Gioco",
          url: "/il-tuo-tour/Gioco",
          color: "#ec6608"
        }, 
        {
          name: "Lifestyle",
          url: "/il-tuo-tour/Lifestyle",
          color: "#819ec3"
        }, 
        {
          name: "Futuro",
          url: "/il-tuo-tour/Futuro",
          color: "#009fe3"
        }, 
        {
          name: "Carta",
          url: "/il-tuo-tour/Carta",
          color: "#13a538"
        },
        {
          name: "Sapore",
          url: "/il-tuo-tour/Sapore",
          color: "#f7bdc3"
        }
      ]
    },
    {
      title: 'Marche d\'Eccellenza',
      url: '/companies',
      icon: 'globe'
    }
  ];

  update: boolean = false;
  joke: any;
  showBtn: boolean = false;
  deferredPrompt: any;
  enabledDiary: boolean;

  constructor(
    private storage: Storage,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public updates: SwUpdate,
    private router: Router,
    private authService: AuthenticationService,
    private alertController: AlertController
  ) {
    this.initializeApp();

    // let status bar overlay webview
    this.statusBar.overlaysWebView(true);
    updates.available.subscribe(event => {

      updates.activateUpdate().then(() => document.location.reload());
    })
    
  }

  ngOnInit() {
    this.storage.get("AUTH_STATE").then((authState) => {
      if(!authState) {
        this.enabledDiary = false;
        console.log('enabledDiary', this.enabledDiary);
      }
      else {
        this.enabledDiary = true;
        console.log('enabledDiary', this.enabledDiary);
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      /*
      this.authService.authState.subscribe((state) => {
        console.log('Auth change: ', state);
        if(state) {
          this.router.navigate(['diary']);
        } else {
          this.router.navigate(['login']);
        }
      })
      */
    });
  }

  toCluster(cluster) {
    console.log(cluster);
    this.router.navigate(['il-tuo-tour/' + cluster.target.id])
  }

  logout() {
    this.authService.logout();
  }
}
