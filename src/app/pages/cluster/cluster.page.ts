import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-cluster',
  templateUrl: './cluster.page.html',
  styleUrls: ['./cluster.page.scss'],
})
export class ClusterPage implements OnInit {
  selectedCluster: string;
  clusterInfo: Array<{ 
    name: string, description: string, previewImage: string 
  }> = [];

  packetsInfo: Array<{ 
    alias: string; title: string; previewImage: string; description: string; priceNotes: string; companies: any[]
  }> = [];

  slidesLuceBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/luce-slide/1.jpg" },
    { src: "../../assets/img/luce-slide/2.jpg" },
    { src: "../../assets/img/luce-slide/3.jpg" },
    { src: "../../assets/img/luce-slide/4.jpg" },
    { src: "../../assets/img/luce-slide/5.jpg" }
  ];

  slidesSuonoBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/suono-slide/1.jpg" },
    { src: "../../assets/img/suono-slide/2.jpg" },
    { src: "../../assets/img/suono-slide/3.jpg" },
    { src: "../../assets/img/suono-slide/4.jpg" }
  ];

  slidesGiocoBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/gioco-slide/1.jpg" },
    { src: "../../assets/img/gioco-slide/2.jpg" }
  ];

  slidesLifestyleBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/lifestyle-slide/1.jpg" },
    { src: "../../assets/img/lifestyle-slide/2.jpg" },
    { src: "../../assets/img/lifestyle-slide/3.jpg" },
    { src: "../../assets/img/lifestyle-slide/4.jpg" }
  ];

  slidesFuturoBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/futuro-slide/1.jpg" }
  ];

  slidesCartaBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/carta-slide/1.jpg" }
  ];

  slidesSaporeBackgroundImg: Array<{ src: string }> = [
    { src: "../../assets/img/sapore-slide/1.jpg" },
    { src: "../../assets/img/sapore-slide/2.jpg" },
    { src: "../../assets/img/sapore-slide/3.jpg" },
    { src: "../../assets/img/sapore-slide/4.jpg" }
  ];

  srcImg: string;

  clusters: Array<{ name: string; }> = [];
  companies: Array<{ name: string; }> = [];
  selectedCompany: any;
  selCluster: any;
  resultsPacket: Array<{}>;
  loading: any;
  USERNAME: any;

  sliderBackgroundConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.5,
    loop: true,
    effect: "cube",
    pager: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router, 
    public platform: Platform,
    private api: ApiService,
    public alertController: AlertController,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) {
      this.selectedCluster = this.activatedRoute.snapshot.paramMap.get('cluster');
      this.srcImg = "../../assets/img/"+this.selectedCluster.toLowerCase()+"-slide";
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getInfoCluster();
    this.getPackets();
    this.getClusters();
    this.getCompanies();
  }

  getInfoCluster() {
    this.api.getInfoCluster(this.selectedCluster).subscribe((res) => {
      this.clusterInfo.push({
        name: res.name,
        description: res.description,
        previewImage: res.previewImage
      });
    })
  }

  getPackets() {
    this.api.getPackets().subscribe((resp) => {
      let companies = [];

      resp.forEach(res => {
        if(res.cluster[0].name == this.selectedCluster) {
          res.companies.forEach(r => {
            companies.push(r.name);
          });

          this.packetsInfo.push({
            alias: res.alias,
            previewImage: res.previewImage,
            title: res.title,
            description: res.description,
            priceNotes: res.priceNotes,
            companies: companies
          });
          companies = [];
        }
        
      });
    })
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      this.clusters.push({name: "---"});
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name
        });
      });
    })
  }

  getCompanies() {
    this.api.getCompanies().subscribe((res) => {
      this.companies.push({name: "---"});
      res.forEach(company => {
        this.companies.push({
          name: company.name
        });
      });
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }
  
  toSelectedPacket(packet) {
    this.router.navigate(['il-tuo-tour/' + this.selectedCluster + '/' + packet.alias]);
  }

  toSelPacketFindTrip(packet, cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.alias + '/' + packet.alias]);
  }

  toFooter() {
    this.router.navigate(['footer']);
  }
  
  onCompaniesChange(company) {
    if(company.detail.value != "---") {
      this.selectedCompany = company.detail.value;
    }
    if(company.detail.value == "---") {
      this.selectedCompany = undefined;
    }
  }

  onClustersChange(cluster) {
    if(cluster.detail.value != "---") {
      this.selCluster = cluster.detail.value;
    }
    if(cluster.detail.value == "---") {
      this.selCluster = undefined;
    }
  }

  findPacket() {
    this.resultsPacket = [];

    this.api.getPackets().subscribe(allPackets => {

      allPackets.forEach(packet => {

        // quando sono selezionati tutt' e due
        if(this.selectedCompany && this.selCluster) {
          packet.companies.forEach(company => {
            packet.cluster.forEach(cluster => {
              if(this.selectedCompany == company.name && this.selCluster == cluster.name) {
                this.resultsPacket.push(packet);
              }
            })
          })
        }

        // quando è selezionato solo company
        if(this.selectedCompany && this.selCluster == undefined) {
          packet.companies.forEach(company => {
            if(this.selectedCompany == company.name) {
              this.resultsPacket.push(packet);
            }
          })
        }

        // quando è selezionato solo cluster
        if(this.selectedCompany == undefined && this.selCluster) {
          packet.cluster.forEach(cluster => {
            if(this.selCluster == cluster.name) {
              this.resultsPacket.push(packet);
            }
          })
        }        
      })
    })
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }

}