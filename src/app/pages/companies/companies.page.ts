import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-companies',
  templateUrl: './companies.page.html',
  styleUrls: ['./companies.page.scss'],
})
export class CompaniesPage implements OnInit {
  companies: Array<{
    logo: string; alias: string; name: string;
  }> = [];
  public companiesToFind: Array<{ name: string; }> = [];
  public clusters: Array<{ name: string; }> = [];

  selectedCompany: any;
  selectedCluster: any;
  resultsPacket: Array<{}>;
  USERNAME: any;

  loading: any;

  constructor(private router: Router, 
    private api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getCompanies();
    this.getClusters();
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      this.clusters.push({name: "---"});
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name
        });
      });
    })
  }

  getCompanies() {
    this.api.getCompanies().subscribe((res) => {
      this.companiesToFind.push({name:"---"});
      res.forEach(company => {
        this.companiesToFind.push({ name: company.name });
        if(company.logo != undefined) {
          this.companies.push({
            logo: company.logo,
            alias: company.alias,
            name: company.name
          })
        }
      });
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  toFooter() {
    this.router.navigate(['footer']);
  }
  
  toSelectedCompany(company) {
    this.router.navigate(['companies/' + company.alias]);
  }

  toSelectedPacket(packet, cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.alias + '/' + packet.alias]);
  }

  onCompanyChange(company) {
    if(company.detail.value != "---") {
      this.selectedCompany = company.detail.value;
    }
    if(company.detail.value == "---") {
      this.selectedCompany = undefined;
    }
  }

  onClusterChange(cluster) {
    if(cluster.detail.value != "---") {
      this.selectedCluster = cluster.detail.value;
    }
    if(cluster.detail.value == "---") {
      this.selectedCluster = undefined;
    }
  }

  findPacket() {
    this.resultsPacket = [];

    this.api.getPackets().subscribe(allPackets => {
      allPackets.forEach(packet => {
        // quando sono selezionati tutt' e due
        if(this.selectedCompany && this.selectedCluster) {
          packet.companies.forEach(company => {
            packet.cluster.forEach(cluster => {
              if(this.selectedCompany == company.name && this.selectedCluster == cluster.name) {
                this.resultsPacket.push(packet);
              }
            })
          })
        }

        // quando è selezionato solo company
        if(this.selectedCompany && this.selectedCluster == undefined) {
          packet.companies.forEach(company => {
            if(this.selectedCompany == company.name) {
              this.resultsPacket.push(packet);
            }
          })
        }

        // quando è selezionato solo cluster
        if(this.selectedCompany == undefined && this.selectedCluster) {
          packet.cluster.forEach(cluster => {
            if(this.selectedCluster == cluster.name) {
              this.resultsPacket.push(packet);
            }
          })
        }        
      })
    })
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }
}