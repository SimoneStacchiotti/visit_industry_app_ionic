import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-company',
  templateUrl: './company.page.html',
  styleUrls: ['./company.page.scss'],
})
export class CompanyPage implements OnInit {
  aliasCompany: any;
  companyInfo = {
    alias: null, cluster: { name: null }, name: null, 
    description: null, site: null, previewPicture: null, logo: null,
    pictures: [],
    pois: []
  };
  USERNAME: any;
  loading: any;

  sliderBackgroundConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.5,
    loop: false,
    effect: "fade",
    pager: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router, 
    private api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) {

    this.aliasCompany = this.activatedRoute.snapshot.paramMap.get('company');
  }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getInfoCompany();
  }

  getInfoCompany() {
    this.api.getInfoCompany(this.aliasCompany).subscribe((res) => {
      this.companyInfo.alias = res.alias;
      this.companyInfo.name = res.name;
      this.companyInfo.description = res.description.replace('&nbsp;','');
      this.companyInfo.site = res.site;
      this.companyInfo.logo = res.logo.replace('poi/GetPoiFile','Company/GetCompanyFile');
      this.companyInfo.previewPicture = res.previewPicture;

      if(res.clusters == undefined || 
        res.clusters.length == 0) {
          this.companyInfo.cluster = {
            name: ""
          }
      }
      else {
        this.companyInfo.cluster = {
          name: res.clusters[0].name
        }
      }

      res.pictures.forEach(pic => {
        this.companyInfo.pictures.push({
          thumbnail: pic.thumbnail,
          medium: pic.medium,
          large: pic.large
        });
      });

      res.pois.forEach(poi => {
        this.companyInfo.pois.push({
          alias: poi.alias, 
          name: poi.name, 
          description: poi.description.substring(0, 30) + "...",
          previewPicture: poi.previewPicture
        });
      });

      console.log(this.companyInfo);
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  toSelectedPoi(poi) {
    this.api.getPackets().subscribe(allPackets => {
      allPackets.forEach((packet) => {
        packet.poIs.forEach((poiPacket) => {
          if(poi.name == poiPacket.name) {
            this.router.navigate(['poi/' + poi.name]);
          }
        }) 
      })
    })
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }
}