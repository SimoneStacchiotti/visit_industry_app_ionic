import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-diary',
  templateUrl: './diary.page.html',
  styleUrls: ['./diary.page.scss'],
})
export class DiaryPage implements OnInit {

  USERNAME: any;
  RELOAD: boolean;
  public NAME: any;
  private ID_USER: any;
  public bookState: any;
  public loading: any;
  public userBooking: Array<{}> = [];

  constructor(private router: Router,
    private storage: Storage,
    public api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { 
    }

  ngOnInit() {
    this.checkAuthState();

    /*
    this.authService.storage.get("DO_RELOAD").then((doReload) => {
      if(doReload) {
        this.storage.set("DO_RELOAD", false).then(() => {
          window.location.reload();
        });
      }
    });
    */
    
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
      this.findUserBooking();
    });
  }

  checkAuthState() {
    this.authService.authState.subscribe((state) => {
      console.log("state -> ", state);
      if(state) {
        this.router.navigate(['diary']);
      } else {
        this.router.navigate(['login']);
      }
    });
  }

  toUserPacket(aliasPacket) {
    this.router.navigate(['diary/' + aliasPacket]);
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  findUserBooking() {
    //this.storage.get("USERNAME").then((username) => { this.USERNAME = username; })
    this.api.getUsers().subscribe((response) => {
      response.forEach(user => {
        if(user.email == this.USERNAME) {
          this.NAME = user.name;
          this.ID_USER = user.id.toString();

          this.api.getUserBooking(this.ID_USER).subscribe((resp) => {
            resp.forEach(usBook => {
              if(usBook.stato == 1) {
                this.bookState = "In Attesa";
              }
              else {
                this.bookState = "Processata";
              }
              usBook.dataPrenotazione = this.formatDate(usBook.dataPrenotazione);
              usBook.dataInizio = this.formatDate(usBook.dataInizio);
              this.userBooking.push(usBook);
            });
          }, (err) => {
            console.log(err);
          })
        }
      });
    }, (err) => {
      console.log(err);
    })
  }

  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }

  formatDate(data) {
    var d = new Date(data);
    var date = d.toJSON().slice(0, 10); 
    var nDate = date.slice(8, 10) + '/'  
                + date.slice(5, 7) + '/'  
                + date.slice(0, 4); 
  
    return nDate;
  }
}
