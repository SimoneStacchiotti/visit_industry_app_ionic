import { Component, OnInit } from '@angular/core';
import { HtmlParser } from '@angular/compiler';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.page.html',
  styleUrls: ['./footer.page.scss'],
})
export class FooterPage implements OnInit {
  public news: Array<{ title: string; content: string; }> = [];
  public clusters: Array<{ name: string; alias: string; }> = [];
  USERNAME: any;
  
  constructor(private api: ApiService,
    private router: Router,
    private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getNews();
    this.getClusters();
  }

  getNews() {
    this.api.getNews().subscribe((res) => {
      res.forEach(n => {
        this.news.push({
          title: n.title,
          content: '<h5>' + n.content + '</h5>'
        });
      });
    })
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name,
          alias: cluster.alias
        });
      });
    })
  }

  toCluster(cluster) {
    console.log(cluster);
    this.router.navigate(['il-tuo-tour/' + cluster.target.id])
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }
}