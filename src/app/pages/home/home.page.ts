import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  joke: any;
  clusters: Array<{ name: string; alias: string; url: string; }> = [];
  slidesImg: Array<{ src: string; }> = [
    { src: "../../assets/img/home-page-slide/1.jpg" },
    { src: "../../assets/img/home-page-slide/2.jpg" },
    { src: "../../assets/img/home-page-slide/3.jpg" },
    { src: "../../assets/img/home-page-slide/4.jpg" },
    { src: "../../assets/img/home-page-slide/5.jpg" },
    { src: "../../assets/img/home-page-slide/6.jpg" },
    { src: "../../assets/img/home-page-slide/7.jpg" },
    { src: "../../assets/img/home-page-slide/8.jpg" },
  ];
  backgroundImg: string = "../../assets/img/_new/png/Territorio4.png";

  USERNAME: any;

  sliderBackgroundConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.5,
    loop: true,
    effect: "cube",
    pager: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  };

  constructor(updates: SwUpdate, 
    private storage: Storage,
    private router: Router,
    public navCtrl: NavController,
    private authService: AuthenticationService,
    private api: ApiService) {
    /*
    updates.available.subscribe(event => {
      updates.activateUpdate().then(() => document.location.reload());
    })
    */
  }

  ngOnInit() {
    this.authService.storage.get("DO_RELOAD").then((doReload) => {
      if(doReload) {
        this.storage.set("DO_RELOAD", false).then(() => {
          window.location.reload();
        });
      }
    });

    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getClusters();
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name,
          alias: cluster.alias,
          url: '/il-tuo-tour/' + cluster.alias
        });
      });
    })
  }

  toDiary() {
    this.router.navigate(['diary']);
  }

  toHomePage() {
    this.router.navigate(['home']);
  }

  toCluster(cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.target.id])
  }

  toFooter() {
    this.router.navigate(['footer']);
  }
}