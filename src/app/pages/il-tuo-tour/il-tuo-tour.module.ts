import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IlTuoTourPage } from './il-tuo-tour.page';

const routes: Routes = [
  {
    path: '',
    component: IlTuoTourPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IlTuoTourPage]
})
export class IlTuoTourPageModule {}
