import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IlTuoTourPage } from './il-tuo-tour.page';

describe('IlTuoTourPage', () => {
  let component: IlTuoTourPage;
  let fixture: ComponentFixture<IlTuoTourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IlTuoTourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IlTuoTourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
