import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-il-tuo-tour',
  templateUrl: './il-tuo-tour.page.html',
  styleUrls: ['./il-tuo-tour.page.scss'],
})

export class IlTuoTourPage implements OnInit {

  joke: any;
  btnClusters : Array<{ name: string; }> = [];
  clusters : Array<{ name: string; }> = [];
  companies: Array<{ name: string; }> = [];

  selectedCompany: any;
  selectedCluster: any;
  resultsPacket: Array<{}>;
  USERNAME: any;
  loading: any;
  slidesImg: Array<{ src: string; }> = [
    { src: "../../assets/img/home-page-slide/1.jpg" },
    { src: "../../assets/img/home-page-slide/2.jpg" },
    { src: "../../assets/img/home-page-slide/3.jpg" },
    { src: "../../assets/img/home-page-slide/4.jpg" },
    { src: "../../assets/img/home-page-slide/5.jpg" },
    { src: "../../assets/img/home-page-slide/6.jpg" },
    { src: "../../assets/img/home-page-slide/7.jpg" },
    { src: "../../assets/img/home-page-slide/8.jpg" },
  ];

  sliderBackgroundConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.5,
    loop: true,
    effect: "cube",
    pager: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  };

  constructor(
    private router: Router,
    private api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getClusters();
    this.getCompanies();
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      this.clusters.push({name: "---"});
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name
        });
        this.btnClusters.push({
          name: cluster.name
        });
      });
    })
  }

  getCompanies() {
    this.api.getCompanies().subscribe((res) => {
      this.companies.push({name: "---"});
      res.forEach(company => {
        this.companies.push({
          name: company.name
        });
      });
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  toSelectedCluster(cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.name]);
  }

  toSelectedPacket(packet, cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.alias + '/' + packet.alias]);
  }

  toFooter() {
    this.router.navigate(['footer']);
  }

  onCompanyChange(company) {
    if(company.detail.value != "---") {
      this.selectedCompany = company.detail.value;
    }
    if(company.detail.value == "---") {
      this.selectedCompany = undefined;
    }
  }

  onClusterChange(cluster) {
    if(cluster.detail.value != "---") {
      this.selectedCluster = cluster.detail.value;
    }
    if(cluster.detail.value == "---") {
      this.selectedCluster = undefined;
    }
  }

  findPacket() {
    this.resultsPacket = [];

    this.api.getPackets().subscribe(objPackets => {

      objPackets.forEach((packet) => {

        // quando sono selezionati tutt' e due
        if(this.selectedCompany && this.selectedCluster) {
          packet.companies.forEach(company => {
            packet.cluster.forEach(cluster => {
              if(this.selectedCompany == company.name && this.selectedCluster == cluster.name) {
                this.resultsPacket.push(packet);
              }
            })
          })
        }

        // quando è selezionato solo company
        if(this.selectedCompany && this.selectedCluster == undefined) {
          packet.companies.forEach(company => {
            if(this.selectedCompany == company.name) {
              this.resultsPacket.push(packet);
            }
          })
        }

        // quando è selezionato solo cluster
        if(this.selectedCompany == undefined && this.selectedCluster) {
          packet.cluster.forEach(cluster => {
            if(this.selectedCluster == cluster.name) {
              this.resultsPacket.push(packet);
            }
          })
        }        
      })
    })
  }

  async presentLoading(msg) {
    this.loading = await this.loadingCtrl.create({
      message: msg
    });
    return await this.loading.present();
  }
}