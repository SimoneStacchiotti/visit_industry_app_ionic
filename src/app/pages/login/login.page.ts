import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  email: string;
  password: string;

  error_messages = {
    'email': [
      { type: 'required', message: 'L\'email è obbligatoria' },
      { type: 'minLength', message: 'L\'email deve avere almeno 6 caratteri' },
      { type: 'maxLength', message: 'L\'email può avere massimo 40 caratteri' },
      { type: 'pattern', message: 'Email non valida' }
    ],
    'password': [
      { type: 'required', message: 'La password è obbligatoria' },
      { type: 'minLength', message: 'La password deve avere almeno 4 caratteri' },
      { type: 'maxLength', message: 'Il telefono può avere massimo 16 caratteri' },
    ]
  }

  constructor(
    public formBuilder: FormBuilder, 
    public storage: Storage,
    public api: ApiService, 
    public alertController: AlertController,
    public router: Router,
    private authService: AuthenticationService,
    private platform: Platform) {

      this.loginForm = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(40),
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        password: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(16)
        ]))
      })
  }

  ngOnInit() {
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      message: msg,
      cssClass: 'danger',
      buttons: ['OK']
    });

    await alert.present();
  }

  getEmail(email) {
    this.email = email.detail.value;
  }

  getPassword(psw) {
    this.password = psw.detail.value;
  }

  login() {
    this.authService.login(this.email, this.password, "password");
  }

  anonymousLogin() {
    this.authService.authState.next(false);
    this.storage.set("AUTH_STATE", false);
    this.router.navigate(['welcome']);
  }

  logout() {
    this.authService.logout();
  }

  toHome(userEmail) {
    this.router.navigate(['welcome'], { queryParams: { email: userEmail } } );
  }
}
