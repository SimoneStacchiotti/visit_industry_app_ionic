import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AgmCoreModule } from '@agm/core';

import { PacketPage } from './packet.page';

const routes: Routes = [
  {
    path: '',
    component: PacketPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAi-WN1CoV2yqsASvTYWzYE5YNb-BzeV_0'
    })
  ],
  declarations: [
    PacketPage
  ]
})
export class PacketPageModule {}
