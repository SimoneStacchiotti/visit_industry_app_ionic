import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketPage } from './packet.page';

describe('PacketPage', () => {
  let component: PacketPage;
  let fixture: ComponentFixture<PacketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacketPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
