import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

var fixedHeader = new Headers();
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

@Component({
  selector: 'app-packet',
  templateUrl: './packet.page.html',
  styleUrls: ['./packet.page.scss'],
})
export class PacketPage implements OnInit {
  lats: Array<number> = [];
  lngs: Array<number> = [];
  height = 300;

  aliasPacket: any;
  cluster: any;
  packetPois: Array<{ previewPicture: string; name: string; description: string;
                    latitude: number; longitude: number; }> = []; 
  packetCompanies: Array<{ name: string; alias: string }> = [];
  packetPictures: Array<{ thumbnail: string; medium: string; large: string; }> = [];
  packetInfo = {
    title: null, programme: null, days: [], price: null, priceNotes: null,
    includedInPrice: null, excludedFromPrice: null, description: null, 
    availabilityPeriod: null, 
  };
  selectedCompany: any;
  selectedCluster: any;
  resultsPacket: Array<{}>;
  clusters: Array<{ name: string; }> = [];
  companies: Array<{ name: string; }> = [];
  USERNAME: any;
  loading: any;
  
  sliderConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.7,
    loop: false,
    effect: "fade",
    pager: true
  };

  sliderBackgroundConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.5,
    loop: false,
    effect: "fade",
    pager: true,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { 
      this.aliasPacket = this.activatedRoute.snapshot.paramMap.get('packet');
      this.cluster = this.activatedRoute.snapshot.paramMap.get('cluster');
  }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getInfoPacket();
    this.getClusters();
    this.getCompanies();
  }

  ngAfterViewInit() {
  }

  /*
  initMap() {
    this.coords = new google.maps.LatLng(43.3230509, 11.9298215);
    this.mapOptions = {
      center: this.coords,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.HYBRID
    }
  }
  */

  getInfoPacket() {
    this.api.getInfoPacket(this.aliasPacket).subscribe((res) => {
      this.packetInfo.title = res.title;
      this.packetInfo.programme = res.programme;
      this.packetInfo.days = res.days;
      this.packetInfo.description = res.description;
      this.packetInfo.availabilityPeriod = res.availabilityPeriod;

      if(res.includedInPrice) {
        this.packetInfo.includedInPrice = res.includedInPrice.replace('&nbsp;','');
      }
      if(res.excludedFromPrice) {
        this.packetInfo.excludedFromPrice = res.excludedFromPrice.replace('&nbsp;','');
      }

      this.packetInfo.price = res.price;
      this.packetInfo.priceNotes = res.priceNotes;
      
      res.companies.forEach(company => {
        this.packetCompanies.push({
          name: company.name,
          alias: company.alias
        });
      });

      res.poIs.forEach(poi => {
        this.packetPois.push({
          previewPicture: poi.previewPicture,
          name: poi.name,
          description: poi.description.substring(0, 30) + "...",
          latitude: poi.latitude,
          longitude: poi.longitude
        })
      });

      res.pictures.forEach(pic => {
        this.packetPictures.push({
          thumbnail: pic.thumbnail,
          medium: pic.medium,
          large: pic.large
        })
      })

      console.log(this.packetPois);
    })
  }

  getClusters() {
    this.api.getClusters().subscribe((res) => {
      this.clusters.push({name: "---"});
      res.forEach(cluster => {
        this.clusters.push({
          name: cluster.name
        });
      });
    })
  }

  getCompanies() {
    this.api.getCompanies().subscribe((res) => {
      this.companies.push({name: "---"});
      res.forEach(company => {
        this.companies.push({
          name: company.name
        });
      });
    })
  }

  toCompany(company) {
    this.router.navigate(['companies/' + company.target.id]);
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }
  
  toFooter() {
    this.router.navigate(['footer']);
  }
  
  toBook(aliasPacket) {
    this.router.navigate(['il-tuo-tour/' + this.cluster + '/' + aliasPacket + '/prenota']);
  }

  toSelectedPacket(packet, cluster) {
    this.router.navigate(['il-tuo-tour/' + cluster.alias + '/' + packet.alias]);
  }

  toSelectedPoi(poi) {
    this.api.getPackets().subscribe(allPackets => {
      allPackets.forEach((packet) => {
        packet.poIs.forEach((poiPacket) => {
          if(poi.name == poiPacket.name) {
            this.router.navigate(['poi/' + poi.name]);
          }
        }) 
      })
    })
  }

  onCompanyChange(company) {
    if(company.detail.value != "---") {
      this.selectedCompany = company.detail.value;
    }
    if(company.detail.value == "---") {
      this.selectedCompany = undefined;
    }
  }

  onClusterChange(cluster) {
    if(cluster.detail.value != "---") {
      this.selectedCluster = cluster.detail.value;
    }
    if(cluster.detail.value == "---") {
      this.selectedCluster = undefined;
    }
  }

  findPacket() {
    this.resultsPacket = [];

    this.api.getPackets().subscribe(allPackets => {

      allPackets.forEach(packet => {

        // quando sono selezionati tutt' e due
        if(this.selectedCompany && this.selectedCluster) {
          packet.companies.forEach(company => {
            packet.cluster.forEach(cluster => {
              if(this.selectedCompany == company.name && this.selectedCluster == cluster.name) {
                this.resultsPacket.push(packet);
              }
            })
          })
        }

        // quando è selezionato solo company
        if(this.selectedCompany && this.selectedCluster == undefined) {
          packet.companies.forEach(company => {
            if(this.selectedCompany == company.name) {
              this.resultsPacket.push(packet);
            }
          })
        }

        // quando è selezionato solo cluster
        if(this.selectedCompany == undefined && this.selectedCluster) {
          packet.cluster.forEach(cluster => {
            if(this.selectedCluster == cluster.name) {
              this.resultsPacket.push(packet);
            }
          })
        }        
      });
    })
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }
}