import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-poi',
  templateUrl: './poi.page.html',
  styleUrls: ['./poi.page.scss'],
})
export class PoiPage implements OnInit {
  poi: any;
  infoPoi: { img: string, name: string, description: string } = {
    img: "", name: "", description: ""
  };
  USERNAME: any;
  loading: any;

  constructor(private activatedRoute: ActivatedRoute, 
    private router: Router,
    private api: ApiService,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { 
      this.poi = this.activatedRoute.snapshot.paramMap.get('poi');
    }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getPois();
  }

  getPois() {
    this.api.getPois().subscribe((allPois) => {
      allPois.forEach((poiPacket) => {
        if(this.poi == poiPacket.name) {
          this.infoPoi = {
            img: poiPacket.previewPicture.replace('dimension=THUMBNAIL','dimension=LARGE'),
            name: poiPacket.name,
            description: poiPacket.description.replace('&nbsp;','')
          }
        }
      }) 
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  toFooter() {
    this.router.navigate(['footer']);
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }
}
