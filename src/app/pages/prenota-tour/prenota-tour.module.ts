import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DatePipe } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { PrenotaTourPage } from './prenota-tour.page';

const routes: Routes = [
  {
    path: '',
    component: PrenotaTourPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PrenotaTourPage],
  providers: [
    DatePicker,
    DatePipe
  ]
})
export class PrenotaTourPageModule {}
