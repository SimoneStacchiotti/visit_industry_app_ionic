import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotaTourPage } from './prenota-tour.page';

describe('PrenotaTourPage', () => {
  let component: PrenotaTourPage;
  let fixture: ComponentFixture<PrenotaTourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrenotaTourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrenotaTourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
