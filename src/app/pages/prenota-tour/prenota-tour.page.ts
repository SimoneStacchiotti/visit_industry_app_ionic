import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DatePipe } from '@angular/common';
import { Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

declare var $;
var gNumPassenger = 1;
@Component({
  selector: 'app-prenota-tour',
  templateUrl: './prenota-tour.page.html',
  styleUrls: ['./prenota-tour.page.scss'],
})
export class PrenotaTourPage implements OnInit {
  categoriaHotel: Array<{}> = ["3 stelle","4 stelle","5 stelle"];
  trattamento: Array<{}> = ["MEZZA PENSIONE", "PENSIONE COMPLETA", "BED & BREAKFAST"];
  sistemazione: Array<{}> = ["DOPPIA", "SINGOLA", "TRIPLA"];
  numPassenger: number = 1;
  selAliasPacket: string;
  titlePacket: string;
  idPacket: number;
  dataPartenza: string
  dataRitorno: string;
  bookForm: FormGroup;
  USERNAME: any;
  loading: any;

  category: any;
  treatment: any;
  place: any;
  
  error_messages = {
    'email': [
      { type: 'required', message: 'L\'email è obbligatoria' },
      { type: 'minLength', message: 'L\'email deve avere almeno 6 caratteri' },
      { type: 'maxLength', message: 'L\'email può avere massimo 40 caratteri' },
      { type: 'pattern', message: 'Email non valida' }
    ],
    'phone': [
      { type: 'required', message: 'Il numero di telefono è obbligatorio' },
      { type: 'minLength', message: 'Il telefono deve avere almeno 8 numeri' },
      { type: 'maxLength', message: 'Il telefono può avere massimo 15 numeri' },
    ],
    'name': [
      { type: 'required', message: 'Il nome è obbligatorio' }
    ],
    'surname': [
      { type: 'required', message: 'Il cognome è obbligatorio' }
    ],
    'age': [
      { type: 'required', message: 'L\'età è obbligatoria' }
    ]
  }

  constructor(public datePicker: DatePicker, 
    public datePipe: DatePipe, 
    public platform: Platform,
    private router: Router,
    public api: ApiService, 
    private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    private authService: AuthenticationService) { 
      this.selAliasPacket = this.activatedRoute.snapshot.paramMap.get('packet');

      this.bookForm = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(40),
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        phone: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(15),
        ])),
        name: new FormControl('', Validators.compose([
          Validators.required
        ])),
        surname: new FormControl('', Validators.compose([
          Validators.required
        ])),
        age: new FormControl('', Validators.compose([
          Validators.required
        ]))
      })
    }

  ngOnInit() {
    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.api.getPackets().subscribe((allPackets) => {
      allPackets.forEach((packet) => {
        if(packet.alias == this.selAliasPacket) {
          this.titlePacket = packet.title;
          this.idPacket = packet.id;
        }
      })
    })
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      message: msg,
      cssClass: 'danger',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading(msg) {
    this.loading = await this.loadingCtrl.create({
      message: msg
    });
    return await this.loading.present();
  }

  addPassenger() {
    gNumPassenger++;

    const newPassenger: string = `
    <ion-item id="dataPassenger-`+gNumPassenger+`" class="dataPassenger">
      <ion-label class="blue-text" style="font-size: larger;
      font-weight: 600;" position="stacked">
        Dati passeggero #`+gNumPassenger+` *
      </ion-label>

      <ion-input formControlName="name`+gNumPassenger+`" placeholder="Nome" id="nome-`+gNumPassenger+`"></ion-input>
      <ion-input formControlName="surname`+gNumPassenger+`" placeholder="Cognome" id="cognome-`+gNumPassenger+`"></ion-input>
      <ion-input formControlName="age`+gNumPassenger+`" placeholder="Età" id="eta-`+gNumPassenger+`" type="number"></ion-input>
    </ion-item>
    `;

    $(newPassenger).insertBefore($('#buttons'));
  }

  removePassenger() {
    if(gNumPassenger != 1) {
      $('#dataPassenger-'+gNumPassenger).remove();
      gNumPassenger--;
    }
  }

  selectDate() {
   this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  onCategoriaHotel(category) {
    this.category = category;
  }

  onTrattamento(treatment) {
    this.treatment = treatment;
  }

  onSistemazione(place) {
    this.place = place;
  }

  onDataPartenza(data) {
    //this.dataPartenza = formatDate(data);
    this.dataPartenza = data.detail.value;
  }

  onDataRitorno(data) {
    //this.dataRitorno = formatDate(data);
    this.dataRitorno = data.detail.value;
  }

  bookTour() {
    let partecipanti = [{}];
    let numPassengerErr;

    $('.dataPassenger').each((i, passenger) => {
      if(passenger.children[1].value != "" 
        && passenger.children[2].value != ""
        && passenger.children[3].value != "") {
        partecipanti.push({
          nome: passenger.children[1].value,
          cognome: passenger.children[2].value,
          eta: passenger.children[3].value,
        });
      }
      else {
        numPassengerErr = i+1;
        partecipanti = [];
      }

    });

    if(partecipanti.length == 0) {
      let msg = 'Dati Passaggero #'+numPassengerErr+' incompleti.';
      this.presentAlert(msg);
      return;
    }

    if(this.dataPartenza > this.dataRitorno) {
      let msg = 'Il ritorno non può avvenire prima della partenza. Data Partenza: ' + formatDate(this.dataPartenza) + ' - Data Ritorno: ' + formatDate(this.dataRitorno);
      this.presentAlert(msg);
      return;
    }

    partecipanti.shift();

    let aereoportoPartenza = $('#aereoporto-partenza')[0].value;
    let aereoportoRitorno = $('#aereoporto-ritorno')[0].value;
    let itinirario = $('#itinirario')[0].value;
    let budget = $('#budget')[0].value;
    let city = $('#citta')[0].value;
    let richieste = $('#richieste')[0].value;
    let category = "", treatment = "", place = "";

    if(this.category != undefined) {
      category = this.category.detail.value;
    }
    if(this.treatment != undefined) {
      treatment = this.treatment.detail.value;
    }
    if(this.place != undefined) {
      place = this.place.detail.value;
    }

    let body = {
      "AeroportoPartenza": aereoportoPartenza,
      "AeroportoRitorno": aereoportoRitorno,
      "DataPartenza": this.dataPartenza == undefined ? "" : this.dataPartenza,
      "DataRitorno": this.dataRitorno == undefined ? "" : this.dataRitorno,
      "Itinirario": itinirario,
      "Budget": budget,
      "Email": $('#email').val(),
      "Telefono": $('#telefono').val(),
      "Citta": city,
      "Pacchetto": this.titlePacket,
      "IdPacchetto": this.idPacket.toString(),
      "CategoriaHotel": category,
      "Trattamento": treatment,
      "Sistemazione": place,
      "Richieste": richieste,
      "Partecipanti": partecipanti
    };
    
    console.log(body);
    
    
    this.api.postBook(body).subscribe((resp : {Code: number, Message: string}) => {
        let msg = '';
        if(resp.Code == -1) {
          msg = resp.Message;
        }
        else {
          msg = 'Mail inviata correttamente';
        }
        this.presentAlert(msg);
      }, error => {
        console.log(error);
        this.presentAlert(error);
      });
    
  }
}

function formatDate(data) {
  var d = new Date(data.detail.value);
  var date = d.toJSON().slice(0, 10); 
  var nDate = date.slice(8, 10) + '/'  
              + date.slice(5, 7) + '/'  
              + date.slice(0, 4); 

  return nDate;
}