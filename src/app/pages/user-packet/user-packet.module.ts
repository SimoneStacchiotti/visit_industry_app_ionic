import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AgmCoreModule } from '@agm/core';
import { IonicModule } from '@ionic/angular';

import { UserPacketPage } from './user-packet.page';

const routes: Routes = [
  {
    path: '',
    component: UserPacketPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAi-WN1CoV2yqsASvTYWzYE5YNb-BzeV_0'
    })
  ],
  declarations: [UserPacketPage]
})
export class UserPacketPageModule {}
