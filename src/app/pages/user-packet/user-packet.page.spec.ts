import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPacketPage } from './user-packet.page';

describe('UserPacketPage', () => {
  let component: UserPacketPage;
  let fixture: ComponentFixture<UserPacketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPacketPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPacketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
