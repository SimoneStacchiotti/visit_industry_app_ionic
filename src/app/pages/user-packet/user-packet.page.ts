import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-user-packet',
  templateUrl: './user-packet.page.html',
  styleUrls: ['./user-packet.page.scss'],
})
export class UserPacketPage implements OnInit {

  ID_PACKET: number = null;
  USERNAME: any;
  ID_USER: any;
  loading: any;
  height = 300;

  bookInfo: {
    dataPartenza: string; dataRitorno: string; categoriaHotel: string;
    trattamento: string; sistemazione: string;
  } = {
    dataPartenza: "", dataRitorno: "", categoriaHotel: "",
    trattamento: "", sistemazione: ""
  };

  packetInfo: {
    title: string; programme: string; days: Array<any>; companies: Array<any>; 
    cluster: Array<any>; poIs: Array<any>;
    price: string; priceNotes: string; includedInPrice: string; excludedFromPrice: string; 
    description: string; availabilityPeriod: string; 
  } = {
    title: "", programme: "", price: "", priceNotes: "", includedInPrice: "", excludedFromPrice: "", 
    description: "", availabilityPeriod: "",
    days: [], companies: [], 
    cluster: [], poIs: []
  };

  sliderConfig = {
    spaceBetween: 2,
    centeredSlides: true,
    slidesPerView: 1.7,
    loop: false,
    effect: "fade",
    pager: true
  };
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private api: ApiService,
    private router: Router,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private authService: AuthenticationService) { 

    this.ID_PACKET = parseInt(this.activatedRoute.snapshot.paramMap.get('packet'));
  }

  ngOnInit() {
    this.checkAuthState();

    this.authService.storage.get("USERNAME").then((username) => {
      this.USERNAME = username;
    });
    this.getInfoPacket();
  }

  checkAuthState() {
    /*
    this.authService.authState.subscribe((state) => {
      console.log("state -> ", state);
      if(state) {
        this.router.navigate(['diary']);
      } else {
        this.router.navigate(['login']);
      }
    });
    */
    this.authService.storage.get("AUTH_STATE").then((authState) => {
      if(!authState) {
        this.router.navigate(['login']);
      }
    });
  }

  getInfoPacket() {
    this.api.getInfoPacketById(this.ID_PACKET).subscribe(async (packet) => {
      packet.poIs.forEach(poi => {
        poi.description = poi.description.substring(0, 30) + "...";
      });
      this.packetInfo.days = packet.days;
      this.packetInfo.companies = packet.companies;
      this.packetInfo.cluster = packet.cluster;
      this.packetInfo.poIs = packet.poIs;
      this.packetInfo.title = packet.title;
      this.packetInfo.programme = packet.programme;
      this.packetInfo.price = packet.price;
      this.packetInfo.priceNotes = packet.priceNotes;
      this.packetInfo.includedInPrice = packet.includedInPrice;
      this.packetInfo.excludedFromPrice = packet.excludedFromPrice;
      this.packetInfo.description = packet.description;
      this.packetInfo.availabilityPeriod = packet.availabilityPeriod;

      console.log(this.packetInfo);

      //this.storage.get("USERNAME").then((username) => { this.USERNAME = username; })
      this.api.getUsers().subscribe((response) => {
        response.forEach(user => {
          if(user.email == this.USERNAME) {
            this.ID_USER = user.id.toString();

            this.api.getUserBooking(this.ID_USER).subscribe((response) => {
              response.forEach(res => {
                if(res.dataPartenza != null) {
                  this.bookInfo.dataPartenza = this.formatDate(res.dataPartenza);
                }
                if(res.dataPartenza == null) {
                  this.bookInfo.dataPartenza == undefined;
                }
                if(res.dataRitorno != null) {
                  this.bookInfo.dataRitorno = this.formatDate(res.dataRitorno);
                }
                if(res.dataRitorno == null) {
                  this.bookInfo.dataRitorno == undefined;
                }
                this.bookInfo.categoriaHotel = res.categoriaHotel;
                this.bookInfo.trattamento = res.trattamento;
                this.bookInfo.sistemazione = res.sistemazione;
              });
            })
          }
        });

      }, (err) => {
        console.log(err);
      })
    })
  }

  async getBookInfo() {
    
  }

  toSelectedPoi(poi) {
    this.api.getPackets().subscribe(allPackets => {
      allPackets.forEach((packet) => {
        packet.poIs.forEach((poiPacket) => {
          if(poi.name == poiPacket.name) {
            this.router.navigate(['poi/' + poi.name]);
          }
        }) 
      })
    })
  }

  /* UTILS */
  async presentLoading(msg, spinner) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
      spinner: spinner
    });
    return await this.loading.present();
  }

  formatDate(data: string) {
    var d = new Date(data);
    var date = d.toJSON().slice(0, 10); 
    var nDate = date.slice(8, 10) + '/'  
                + date.slice(5, 7) + '/'  
                + date.slice(0, 4); 
  
    return nDate;
  }

  toHomePage() {
    this.router.navigate(['welcome']);
  }
}
