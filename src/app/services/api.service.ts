import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //https://www.visitindustry-marche.it
  private baseUri: string = "https://www.visitindustry-marche.it";
  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      })
  };

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService,
    private platform: Platform) { 

      this.platform.ready().then(() => {})
  }

  getCompanies() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Companies');
  }

  getInfoCompany(aliasCompany: any) {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Company/' + aliasCompany);
  }

  getClusters() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Tags');
  }

  getInfoCluster(selectedCluster: any) {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Tag/' + selectedCluster);
  }

  getPackets() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Packets');
  }

  getInfoPacket(aliasPacket: any) {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Packet/' + aliasPacket);
  }

  getInfoPacketById(id: number) {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/PacketById/' + id);
  }

  getPois() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/POIs');
  }

  getNews() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/News');
  }

  getUsers() {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/Users');
  }

  getUserBooking(id: any) {
    //this.authService.isExpiredToken();
    return this.http.get<any>(this.baseUri + '/api/UserBooking/' + id);
  }

  postBook(body) {
    return this.http.post(this.baseUri + '/api/book', body, this.httpOptions);
  }
}
