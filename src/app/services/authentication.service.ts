import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform, AlertController, BooleanValueAccessor } from '@ionic/angular';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private ACCESS_TOKEN: any;
  private REFRESH_TOKEN: any;
  private CLIENT_SECRET: string = '@|Tc_fQXYAZ^3LisM0hzfcnGX36F3|&z9_A$@@1?8i0V##G0ZxLqsoMVIzJqsbU=y7-YnFSzi';
  private CLIENT_ID: string = 'js';
  private GRANT_TYPE: string;

  private DO_RELOAD: boolean;
  private USERNAME: any;
  private PASSWORD: any;
  private AUTH_STATE: any;
  //thisIsTheL0ng&DIFFp@55

  //https://www.visitindustry-marche.it
  private baseUri: string = "https://www.visitindustry-marche.it";
  private tokenLoginApiUri: string = this.baseUri + '/api/token';

  authState = new BehaviorSubject(false);

  constructor(
    public storage: Storage, 
    private platform: Platform,
    private http: HttpClient,
    public alertController: AlertController,
    private router: Router) {
    this.platform.ready().then(() => {
      this.USERNAME = this.storage.get("USERNAME");
      this.PASSWORD = this.storage.get("PASSWORD");
      this.REFRESH_TOKEN = this.storage.get("REFRESH_TOKEN");
      this.GRANT_TYPE = 'password';
      this.checkToken();
    })
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      message: msg,
      cssClass: 'danger',
      buttons: ['OK']
    });

    await alert.present();
  }

  login(username: string, password: string, grant_type: string): void {
    let headerHttp = this.setHeaderToken();
    let bodyHttp = this.setBody(username, password, grant_type);
    
    this.http.post<any>(this.tokenLoginApiUri, bodyHttp.toString(), { headers: headerHttp } )
    .subscribe((resp) => {
      this.authState.next(true);
      this.storage.set("AUTH_STATE", true).then(() => {
        this.storeUser(username, password);
        this.storeTokens(resp.access_token, resp.refresh_token);
        this.router.navigate(['diary']);
      });
      
    }, (err) => {
      // bad request
      if(err.status == 400) {
        let msg = "Username e/o password errata. Login non riuscito.";
        this.presentAlert(msg);
      }
      return err;
    });
  }

  logout(): any {
    this.removeTokens();
    this.authState.next(false);
    this.router.navigate(['login']);
  }

  setHeaderToken(): HttpHeaders {
    let header = new HttpHeaders();
    header.append('Content-Type','application/x-www-form-urlencoded');
    return header;
  }

  setHeader() {
    let header = new HttpHeaders();
    header.append('Content-Type','application/json');
    header.append('Accept','application/json');
    return header;
  }

  setBody(username, password, grant_type): URLSearchParams {
    let body = new URLSearchParams();
    body.append('grant_type', grant_type);
    body.append('username', username);
    body.append('password', password);
    body.append('client_id', this.CLIENT_ID);
    body.append('client_secret', this.CLIENT_SECRET);
    body.append('refresh_token', this.REFRESH_TOKEN);

    return body;
  }

  getUsername() {
    return this.USERNAME;
  }

  storeUser(username, password) {
    this.USERNAME = username;
    this.PASSWORD = password;
    this.AUTH_STATE = this.authState.value;
    this.DO_RELOAD = true;
    this.storage.set("USERNAME", this.USERNAME);
    this.storage.set("PASSWORD", this.PASSWORD);
    this.storage.set("AUTH_STATE", this.AUTH_STATE);
    this.storage.set("DO_RELOAD", this.DO_RELOAD);
  }

  storeTokens(access_token, refresh_token): void {
    this.ACCESS_TOKEN = access_token;
    this.storage.set("ACCESS_TOKEN", this.ACCESS_TOKEN);
    if(refresh_token != undefined) {
      this.REFRESH_TOKEN = refresh_token;
      this.storage.set("REFRESH_TOKEN", this.REFRESH_TOKEN);
    }
  }

  storeToken(access_token): void {
    this.ACCESS_TOKEN = access_token;
    this.storage.set("ACCESS_TOKEN", this.ACCESS_TOKEN);
  }

  removeTokens(): void {
    this.storage.remove("ACCESS_TOKEN");
    this.storage.remove("USERNAME");
    this.storage.remove("PASSWORD");
    this.storage.set("AUTH_STATE", false);
    this.storage.set("DO_RELOAD", true);
    //this.storage.remove("AUTH_STATE");
    //this.storage.remove("DO_RELOAD");
    this.authState.next(false);
  }

  isAuthenticated(): boolean {
    return this.authState.value;
  }

  async checkToken(): Promise<void> {
    return await this.storage.get("ACCESS_TOKEN").then((res) => {
      if(res) {
        this.authState.next(true);
      }
    })
  }

  /**
   * ! check if the token is expired --> api response 401 (unauthorized) !
   * 
   * if yes: 
   * call refreshToken method 
   * change grant_type to 'refresh_token' in header
   * fetch to api to refresh the token
   * store new token
   * 
   * else:
   * do nothing
   */
  isExpiredToken() {
    let headerHttp = this.setHeaderToken();
    let bodyHttp = this.setBody(this.USERNAME, this.PASSWORD, this.GRANT_TYPE);
    
    this.http.post<any>(this.tokenLoginApiUri, bodyHttp.toString(), { headers: headerHttp } )
    .subscribe((resp) => {
      //this.authState.next(true);
    }, (err) => {
      console.log(err);
      // UNAUTHORIZED
      if(err.status == 401) {
        this.refreshToken();
      }
      return err;
    });
    
  }

  refreshToken() {
    this.GRANT_TYPE = 'refresh_token';
    let headerHttp = this.setHeaderToken();

    let bodyHttp = this.setBody(this.USERNAME, this.PASSWORD, this.GRANT_TYPE);
    
    this.http.post<any>(this.tokenLoginApiUri, bodyHttp.toString(), { headers: headerHttp } )
    .subscribe((resp) => {
      this.storeTokens(resp.access_token, resp.refresh_token);
      this.GRANT_TYPE = 'password';
      this.authState.next(true);
    }, (err) => {
      console.log(err);
    });
  }
}
